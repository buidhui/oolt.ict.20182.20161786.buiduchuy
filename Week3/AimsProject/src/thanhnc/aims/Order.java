package thanhnc.aims;

import java.util.ArrayList;
import java.util.List;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	
//	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private List<DigitalVideoDisc> itemsOrder = new ArrayList<DigitalVideoDisc>(MAX_NUMBERS_ORDERED);
	
	private int qtyOrdered;

	public Order() {
		super();
		this.qtyOrdered = 0;
	}

	public int getQtyOrdered() {
		return qtyOrdered;
	}

	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if (this.qtyOrdered <= Order.MAX_NUMBERS_ORDERED) {
//			this.itemsOrdered[this.qtyOrdered] = disc;
			this.itemsOrder.add(disc);
			this.qtyOrdered++;
			System.out.println("The disc has been added");
		} else {
			System.out.println("The disc is full");
		}
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		if (this.qtyOrdered == 0) {
			System.out.println("The order is empty");
		} else {
			this.itemsOrder.remove(disc);
			this.qtyOrdered--;
		}
	}
	
	public double totalCost() {
		double totalCost = 0;
		for (int i = 0; i < this.itemsOrder.size(); ++i) {
			totalCost += this.itemsOrder.get(i).getCost();
		}
		return totalCost;
	}
}
