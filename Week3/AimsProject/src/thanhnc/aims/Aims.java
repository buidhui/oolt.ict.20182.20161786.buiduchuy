package thanhnc.aims;

public class Aims {

	public static void main(String[] args) {
		Order anOrder = new Order();
		// Create a new dvd object  and set the fields
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		// add the dvd to the order
		anOrder.addDigitalVideoDisc(dvd1);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		anOrder.addDigitalVideoDisc(dvd2);
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);
		anOrder.addDigitalVideoDisc(dvd3);
		
		System.out.println("Total cost is: " + anOrder.totalCost());
		
		//remove testing
		System.out.println("Remove dvd3...");
		anOrder.removeDigitalVideoDisc(dvd3);
		System.out.println("Total dvd is: " + anOrder.getQtyOrdered());
	}

}
