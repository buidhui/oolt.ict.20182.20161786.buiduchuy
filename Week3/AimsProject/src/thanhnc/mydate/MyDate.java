package thanhnc.mydate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;
import java.util.TimeZone;

public class MyDate {
	private int day;
	private int month;
	private int year;
	
	//constructor
	String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
	public MyDate() {
		Calendar calendar = Calendar.getInstance(TimeZone.getDefault()); //getTime() returns the current date in default time zone
//	    Date date = calendar.getTime();
	    int day = calendar.get(Calendar.DATE); //Note: +1 the month for current month
	    int month = calendar.get(Calendar.MONTH) + 1;
	    int year = calendar.get(Calendar.YEAR);
	    this.day = day;
	    this.month = month;
	    this.year = year;
	}
	public MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public MyDate(String date) {
		String newDate = "February 18th 2019";
		if (date.contains("st")) {
			newDate = date.replace("st", "");
		} else if (date.contains("nd")) {
			newDate = date.replace("nd", "");
		} else if (date.contains("rd")) {
			newDate = date.replace("rd", "");
		} else if (date.contains("th")) {
			newDate = date.replace("th", "");
		} else {
			System.out.println("String is not valid!");
		}
		
		SimpleDateFormat formatter = new SimpleDateFormat("MMMM dd yyyy");
		try {
			Date dateFormat = formatter.parse(newDate);
			Calendar calendar = new GregorianCalendar();
            calendar.setTime(dateFormat);
            this.year = calendar.get(Calendar.YEAR);
            this.month = calendar.get(Calendar.MONTH) + 1;
            this.day = calendar.get(Calendar.DAY_OF_MONTH);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
	}
	
	//getter and setter
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		if (day > 0 && day < 32) {
			this.day = day;
		} else {
			System.out.println("Invalid day");
		}
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		if (month > 0 && month < 13) {
			this.month = month;
		} else {
			System.out.println("Invalid month");
		}
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		if (year > 0) {
			this.year = year;
		} else {
			System.out.println("Invalid year");
		}
	}
	
	//method
	public static MyDate accept() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a string: (Eg: February 18th 2019)");
		String date = sc.nextLine();
		
		MyDate myDate = new MyDate(date);
		
		return myDate;
	}
	
	public void print() {
		System.out.println(this.getDay() + "-" + this.getMonth() + "-" + this.getYear());
	}
}
