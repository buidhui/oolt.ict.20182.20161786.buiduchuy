package thanhnc.mydate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class MyDateMain {

	public static void main(String[] args) {
		MyDate myDate1 = MyDate.accept();
		myDate1.print();
		
		MyDate myDate2 = new MyDate();
		myDate2.print();
		
		MyDate myDate3 = new MyDate(13,8,2019);
		myDate3.print();
	}

}
