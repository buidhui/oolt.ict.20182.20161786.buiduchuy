import java.util.Scanner;
import java.time.YearMonth;
public class ex5 {

    
  public static void main(String[] strings) {

        Scanner input = new Scanner(System.in);

        System.out.print("Input a month number: ");
        int month = input.nextInt();

        System.out.print("Input a year: ");
        int year = input.nextInt();

        YearMonth yearMonthObject = YearMonth.of(year, month);
        int daysInMonth = yearMonthObject.lengthOfMonth();
        
        System.out.print("It has " + daysInMonth + " days\n");
    }
}