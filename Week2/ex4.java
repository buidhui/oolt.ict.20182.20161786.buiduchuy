import java.util.Scanner;
public class ex4{
	public static void main(String args[]) {
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter n? ");
		int n = keyboard.nextInt();
		
		for(int i = 1; i <= n ; i++ ) {
			for(int j = n; j > i; j--) {
				System.out.print(" ");		
			}
			
			for(int h = 1; h <= 2*i - 1; h++) {
				System.out.print("*");
			}
			System.out.println(" ");
		}
	}
}